package ru.dront78.pulsedroid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class PulseDroidActivity extends AppCompatActivity {

    public static final int DEFAULT_INDEX_BUFFER_HEADROOM = 4;
    public static final int DEFAULT_INDEX_TARGET_LATENCY = 6;

    private static final List<Long> VALUES_BUFFER_HEADROOM =
            Arrays.asList(0L, 15625L, 31250L, 62500L, 125000L, 250000L, 500000L, 1000000L, 2000000L);
    private static final List<Long> VALUES_TARGET_LATENCY =
            Arrays.asList(0L, 15625L, 31250L, 62500L, 125000L, 250000L, 500000L, 1000000L, 2000000L, 5000000L, 10000000L, -1L);

    private Button playButton = null;
    private Spinner bufferHeadroomSpinner;
    private Spinner targetLatencySpinner;
    private BufferSizeAdapter bufferHeadroomAdapter;
    private BufferSizeAdapter targetLatencyAdapter;
    private EditText serverInput;
    private EditText portInput;
    private CheckBox autoStartCheckBox = null;
    private TextView errorText;

    private PulsePlaybackService boundService;

    private final ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            boundService = ((PulsePlaybackService.LocalBinder) service).getService();

            boundService.playState().observe(PulseDroidActivity.this,
                    playState -> updatePlayState(playState));

            boundService.showNotification();

            updatePrefs(boundService);

            if (boundService.getAutostartPref() && boundService.isStartable()) {
                play();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            boundService.playState().removeObservers(PulseDroidActivity.this);

            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            boundService = null;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TextView buildTimeLabel = findViewById(R.id.buildTimeLabel);
        serverInput = findViewById(R.id.EditTextServer);
        portInput = findViewById(R.id.EditTextPort);
        autoStartCheckBox = findViewById(R.id.auto_start);
        playButton = findViewById(R.id.ButtonPlay);
        errorText = findViewById(R.id.errorText);
        bufferHeadroomSpinner = findViewById(R.id.bufferHeadroomSpinner);
        targetLatencySpinner = findViewById(R.id.targetLatencySpinner);

        bufferHeadroomAdapter = new BufferSizeAdapter(this, VALUES_BUFFER_HEADROOM);
        targetLatencyAdapter = new BufferSizeAdapter(this, VALUES_TARGET_LATENCY);

        long buildTime;
        try {
            buildTime = Long.parseLong(getString(R.string.build_time));
        } catch (NumberFormatException ignored) {
            buildTime = 0;
        }
        buildTimeLabel.setText(DateUtils.formatDateTime(
                this, buildTime, DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));

        playButton.setOnClickListener(v -> {
            if (boundService.getPlayState().isActive()) {
                stop();
            } else {
                play();
            }
        });

        bindService(new Intent(this, PulsePlaybackService.class),
                mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        unbindService(mConnection);
        super.onDestroy();
    }

    private void updatePrefs(PulsePlaybackService service) {
        serverInput.setText(service.getServerPref());
        int port = service.getPortPref();
        portInput.setText(port > 0 ? Integer.toString(port) : "");
        autoStartCheckBox.setChecked(service.getAutostartPref());
        setUpSpinner(bufferHeadroomSpinner, bufferHeadroomAdapter, service.getBufferHeadroom(), DEFAULT_INDEX_BUFFER_HEADROOM);
        setUpSpinner(targetLatencySpinner, targetLatencyAdapter, service.getTargetLatency(), DEFAULT_INDEX_TARGET_LATENCY);
    }

    private void setUpSpinner(Spinner spinner, BufferSizeAdapter adapter, long value, int defaultIndex) {
        spinner.setOnItemSelectedListener(null);

        spinner.setAdapter(adapter);

        int pos = adapter.getItemPosition(value);
        spinner.setSelection(pos >= 0 ? pos : defaultIndex);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (boundService != null) {
                    long headroomUsec = bufferHeadroomAdapter.getItem(bufferHeadroomSpinner.getSelectedItemPosition());
                    long latencyUsec = targetLatencyAdapter.getItem(targetLatencySpinner.getSelectedItemPosition());
                    boundService.setBufferUsec(headroomUsec, latencyUsec);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void updatePlayState(@Nullable PlayState playState) {
        if (playState == null) {
            playButton.setText(R.string.btn_waiting);
            playButton.setEnabled(false);
            return;
        }
        switch (playState) {
            case STOPPED:
                playButton.setText(R.string.btn_play);
                playButton.setEnabled(true);
                break;
            case STARTING:
                playButton.setText(R.string.btn_starting);
                playButton.setEnabled(true);
                break;
            case BUFFERING:
                playButton.setText(R.string.btn_buffering);
                playButton.setEnabled(true);
                break;
            case STARTED:
                playButton.setText(R.string.btn_stop);
                playButton.setEnabled(true);
                break;
            case STOPPING:
                playButton.setText(R.string.btn_stopping);
                playButton.setEnabled(false);
                break;
        }
        Throwable error = boundService == null ? null : boundService.getError();
        if (error != null) {
            String text = formatMessage(error);
            errorText.setText(getString(R.string.play_error, text));
        } else {
            errorText.setText("");
        }
    }

    public void play() {
        String server = serverInput.getText().toString();
        int port;
        try {
            port = Integer.parseInt(portInput.getText().toString());
        } catch (NumberFormatException e) {
            portInput.setError(formatMessage(e));
            return;
        }
        portInput.setError(null);

        if (boundService != null) {
            boundService.setPrefs(server, port, autoStartCheckBox.isChecked());
            boundService.play(server, port);
        }
    }

    public void stop() {
        if (boundService != null) {
            boundService.stop();
        }
    }

    @NonNull
    private String formatMessage(Throwable error) {
        String msg = error.getLocalizedMessage();
        return error.getClass().getName()
                + (msg == null ? "" : ": " + msg);
    }

}
