# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2024-01-13

### Changed
- Updated and reduced dependencies.
- Updated build tooling.

### Fixed
- Notification styling on newer Android versions.

## [1.2.0] - 2021-08-29

### Changed
- All buffer values are now inverses of powers of 2 in seconds (need to
  reselect ones below 0.125s).
- Reduce lowest latency setting to 0.015625s (only represents internal buffer,
  Android system has additional latency).
- Tweak worker loop to reduce latency as much as possible.

## [1.1.0] - 2021-08-29

### Added
- Smaller values for buffer sizes.

### Changed
- minSdkVersion increased to 23.
- More explanatory labels for buffer value selection.
- Faster worker interval.
