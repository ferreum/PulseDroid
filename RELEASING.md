## Releasing
1. Increase version in app/build.gradle:
  - versionName: `"<major>.<minor>.<patch>"`
  - versionCode: `major * 1000000 + minor * 1000 + patch`
2. Insert the new version headline and date in `CHANGELOG.md`, below the
  `## [Unreleased]` line:
  - `## [<major>.<minor>.<patch>] - <year>-<month>-<day>`
3. Commit the changes
  - message: `version <major>.<minor>.<patch>`
4. Create version tag
  - `git tag v<major>.<minor>.<patch>`

## Building
1. Create `keystore.properties` with signing info:
        storeFile=<path to keystore>
        storePassword=<keystore password>
        keyAlias=<key alias>
        keyPassword=<key password>
2. Build apk:
  - ./gradlew clean app:assembleRelease
  apk is located at `app/build/outputs/apk/release/app-release.apk`
